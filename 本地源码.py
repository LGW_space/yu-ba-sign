import requests
import json
import time
import os
import msvcrt
from configparser import ConfigParser


def sign(c, group_id):
    url = "https://yuba.douyu.com/ybapi/topic/sign?timestamp=" + \
        str(int(time.time()/100))

    headers = {
        "authority": "yuba.douyu.com",
        "method": "POST",
        "path": "/ybapi/topic/sign?timestamp="+str(int(time.time()/100)),
        "scheme": "https",
        "accept": "*/*",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
        "content-length": "26",
        "content-type": "application/x-www-form-urlencoded",
        "cookie": c,
        "origin": "https://yuba.douyu.com",
        "referer": "https://yuba.douyu.com/group/"+group_id,
        "sec-ch-ua": '"Chromium";v="92", " Not A;Brand";v="99", "Microsoft Edge";v="92"',
        "sec-ch-ua-mobile": "?0",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36 Edg/92.0.902.67",        "x-csrf-token": "hbxPpC1SYvWyptPnO054gAXkiKtODAZg"
    }
    data = {
        'group_id': group_id
    }
    r = requests.post(url=url, headers=headers, data=data)
    res = json.loads(r.text)
    if res["status_code"] == 1001:
        print("鱼吧号："+group_id+"---"+res["message"])
    elif res["status_code"] == 200:
        print("鱼吧号："+group_id+"---签到成功")
    else:
        print("鱼吧号："+group_id+"---"+str(json.loads(r.text)))


def getSignInfo(c, group_ids):
    ids = group_ids.split(" ")
    for id in ids:
        sign(c, id)


if __name__ == "__main__":
    try:
        if os.path.exists("yubasign.ini"):
            cfg = ConfigParser()
            cfg.read('yubasign.ini')
            ck = cfg.get('default', 'cookie')
            g_ids = cfg.get('default', 'group_ids')
            getSignInfo(ck, g_ids)
        else:
            cookie = input("请输入cookies：")
            group_ids = input("请输入group_ids, 多个id中间用空格隔开：")
            print("将为您自动创建配置文件【yubasign.ini】，下次可自动执行配置文件内容。变更配置可直接修改配置文件。")

            content = """[default]
cookie=%(cookie)s
group_ids=%(group_ids)s
"""
            with open("yubasign.ini", "w+") as f:
                f.write(content % dict(cookie=cookie, group_ids=group_ids))
            getSignInfo(cookie, group_ids)
    
    except Exception as e:
        print(e) 
    finally:
        print("请按任意键退出~")
        ord(msvcrt.getch())

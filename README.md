# 斗鱼鱼吧批量自动签到【本地+云函数】

## 一、使用须知

### 1.1 如何获取cookie

1. 打开浏览器，进入某个主播的鱼吧（已登录状态）。如：https://yuba.douyu.com/group/xxx (xxx为数字)

   ![](https://gitee.com/LGW_space/img/raw/master/default/202108121157219.png)

2. 按F12，进入开发人员工具，点击【网络】选项卡

   ![](https://gitee.com/LGW_space/img/raw/master/default/202108121157217.png)

3. 刷新网页，看见第一条内容名称：xxx（和url最后的数字一样）

   ![](https://gitee.com/LGW_space/img/raw/master/default/202108121157218.png)

4. 找到【标头】->【cookie】右键【cookie】选择复制值就得到了你的cookie。

   ![](https://gitee.com/LGW_space/img/raw/master/default/202108121157215.png)

### 1.2 如何获取group_ids

`group_id`就是鱼吧URL后面的数字，多个数字中间空格隔开。（见1.1中的图片）

## 二、本地版本

### 2.1 成品打包链接：https://wwx.lanzoui.com/i7eNDsj50jg

### 2.2 使用须知：

- 首次使用需要手动输入cookie和group_ids
- 多个group_id中间用空格隔开
- 首次输入完成后，会在本地保存一个名为`yubasign.ini`的配置文件
- 再次打开软件会自动读取配置文件的内容（cookie和group_ids），自动执行签到

### 2.3 软件运行截图：

![](https://gitee.com/LGW_space/img/raw/master/default/202108121239568.png)

### 2.4 软件源码：

[本地源码.py](https://gitee.com/LGW_space/yu-ba-sign/blob/master/%E6%9C%AC%E5%9C%B0%E6%BA%90%E7%A0%81.py)




## 三、云函数版

腾讯云云函数为例子，注册和配置可以查看我以前的一个帖子里面[教你从0搭建微信推送斗鱼直播提醒#五、云函数的开发](https://www.52pojie.cn/thread-1401235-1-1.html#37638540_%E4%BA%94%E3%80%81%E4%BA%91%E5%87%BD%E6%95%B0%E7%9A%84%E5%BC%80%E5%8F%91)，或自行百度相关内容。

直接上代码：(新建python3.6的空模板)

[云函数源码.py](https://gitee.com/LGW_space/yu-ba-sign/blob/master/%E4%BA%91%E5%87%BD%E6%95%B0.py)

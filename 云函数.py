import requests
import json
import time


def sign(c, group_id):
    url = "https://yuba.douyu.com/ybapi/topic/sign?timestamp=" + \
        str(int(time.time()/100))

    headers = {
        "authority": "yuba.douyu.com",
        "method": "POST",
        "path": "/ybapi/topic/sign?timestamp="+str(int(time.time()/100)),
        "scheme": "https",
        "accept": "*/*",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6",
        "content-length": "26",
        "content-type": "application/x-www-form-urlencoded",
        "cookie": c,
        "origin": "https://yuba.douyu.com",
        "referer": "https://yuba.douyu.com/group/"+group_id,
        "sec-ch-ua": '"Chromium";v="92", " Not A;Brand";v="99", "Microsoft Edge";v="92"',
        "sec-ch-ua-mobile": "?0",
        "sec-fetch-dest": "empty",
        "sec-fetch-mode": "cors",
        "sec-fetch-site": "same-origin",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36 Edg/92.0.902.67",        "x-csrf-token": "hbxPpC1SYvWyptPnO054gAXkiKtODAZg"
    }
    data = {
        'group_id': group_id
    }
    r = requests.post(url=url, headers=headers, data=data)
    res = json.loads(r.text)
    if res["status_code"] == 1001:
        print("鱼吧号："+group_id+"---"+res["message"])
    elif res["status_code"] == 200:
        print("鱼吧号："+group_id+"---签到成功")
    else:
        print("鱼吧号："+group_id+"---"+str(json.loads(r.text)))


def getSignInfo(c, group_ids):
    for id in group_ids:
        sign(c, str(id))


def main_handler(event, context):
    try:
        cookie = "dy_did=" # 替换成你的cookie
        group_ids = [561, 515, 3270215, 123213123] # 替换成你的group_ids

        getSignInfo(cookie, group_ids)
    
    except Exception as e:
        print(e) 